"""
Runnable file
"""
from tasks.classifier import Classification


if __name__ == '__main__':
    objClf = Classification()
    objClf.train_classifier()
    objClf.score()