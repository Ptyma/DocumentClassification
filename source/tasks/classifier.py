"""
Classification class with classifier and visualization of predicted data.
"""

from .vectorizer import DataVectorizer

from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score,precision_score,recall_score,f1_score

import random
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import numpy as np


class Classification(object):
    def __init__(self):
        """

        """

        self.clfObj = MultinomialNB()
        self.tr_vector, self.te_vector, self.training_label, self.testing_label = DataVectorizer().data_vectorization()
        self.pred = None

    def train_classifier(self):
        """
        Fits the data to MultinomialNB
        :return:
        """

        self.clfObj.fit(self.tr_vector, self.training_label)

    def score(self):
        """
        Does the calculations.
        :return:
        """

        self.pred = self.clfObj.predict(self.te_vector)

        print('Accuracy score: {}'.format(accuracy_score(self.testing_label,
                            self.pred)))
        print('Precision score: {}'.format(precision_score(self.testing_label,
                            self.pred, pos_label=1,average='weighted')))
        print('Recall score: {}'.format(recall_score(self.testing_label,
                            self.pred, pos_label=1,average='weighted')))
        print('F1 score: {}'.format(f1_score(self.testing_label, self.pred,
                            pos_label=1,average='weighted')))

        self.visualization()

    def visualization(self):
        """
        Creates a bar graph for the predicted data.
        :return:
        """
        doc_types = set(self.pred)
        type_count = {}

        for doc_type in doc_types:
            type_count[doc_type] = len([item for item in self.pred if
                                        item == doc_type])

        plt.subplots()

        opacity = 0.4

        for key, value in type_count.items():
            number_of_colors = 8

            color = ["#" + ''.join(
                [random.choice('0123456789ABCDEF') for j in range(6)])
                     for i in range(number_of_colors)]
            plt.bar(key, value,
                    alpha=opacity,
                    color=color,
                    label=key)


        plt.xlabel('Types')
        plt.ylabel('Data-Count')
        plt.title('Predicted by Algorithm')

        plt.legend()

        plt.savefig('bar.png')